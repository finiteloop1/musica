# Musica

MUSICA is a social networking App for Music Lovers. The basic notion of this app is to provide a platform for Music Enthusiasts to share their own creations and to like and follow their idol or friends and their Music on this Social Network.
Not only that, we can also have a Private Playlist which will only be visible to us, 
also, there will be our Friend Circles or Rooms in which we can collaborate, chat and find what our fellow friends are listening to and know their taste of music, 
also, you can know what is trending around the world and in your circles.
Icons used in this app are icons made by Freepik from www.flaticon.com.

Tools and Frameworks Used :  
1. Firebase Authentication
2. Firebase Realtime Database  
3. Firebase Storage . 
4. Nodejs . 
5. Restify . 
6. Firebase UI . 
7. RxJAVA . 
8. Forever . 
9. Heroku . 
10. Volley . 
11. Java Servlet . 
12. MongoDB . 
13. Restify . 

Note: You need Internet Connection to run this app :)